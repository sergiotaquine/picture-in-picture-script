# Picture In Picture Script

## Description 
Add a top bar with a button to activate the brand new chrome's feature called `Picture in Picture` on the first video we found in the page. Normally works on every page which have a `video` markup inside.

## How to use it
To use this script I use Tampermonkey.

*   Download [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=fr) Chrome extension on the official Chrome Web store.
*   Simply add [the raw script](https://gitlab.com/sergiotaquine/picture-in-picture-script/raw/master/PipActivator.js) to a new userscript in Tampermonkey.


**Please if you can read code, read it first and never add a script you find on the web if you don't trust the content or the developper who did it.**

> If you're lost here and don't know how the web works : Scripts could be very dangerous. 
