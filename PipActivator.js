// ==UserScript==
// @name         PIP Video
// @namespace    http://tampermonkey.net/
// @version      0.4
// @description  Add a top bar to activate Picture in Picture on the first video we found in the page.
// @author       Sergio Taquine
// @author       David Gribouille
// @match        https://*/*
// @grant        none
// @downloadURL  https://gitlab.com/sergiotaquine/picture-in-picture-script/raw/master/PipActivator.js
// @updateURL    https://gitlab.com/sergiotaquine/picture-in-picture-script/raw/master/PipActivator.js
// ==/UserScript==

(function() {
    'use strict';
    if (window.top != window.self) { //-- Don't run on frames or iframes
        return;
    }
    const body = window.top.document.body;

    setTimeout(() => {
        let videos = document.getElementsByTagName('video');
        if (videos.length !== 0) {
            let overloadedBar = document.createElement('div');
            overloadedBar.style.position = 'fixed';
            overloadedBar.style.top= '0';
            overloadedBar.style.left= '0';
            overloadedBar.style.right= '0';
            overloadedBar.style.height= '50px';
            overloadedBar.style.backgroundColor= 'rgba(0,0,0,.5)';
            overloadedBar.style.display= 'flex';
            overloadedBar.style.justifyContent= 'center';
            overloadedBar.style.alignItems= 'center';
            overloadedBar.style.zIndex= '9000';

            let pipButton = document.createElement('button');
            pipButton.setAttribute("id", "button-setpip");
            pipButton.style.padding = '8px 16px';
            pipButton.style.borderRadius = '5px';
            pipButton.style.backgroundColor = '#34a1eb';
            pipButton.style.zIndex = '9000';
            pipButton.style.fontFamily = "'Futura', 'Helvetica Neue', Helvetica, Arial, 'sans-serif'";
            pipButton.style.border= 'none';
            pipButton.innerHTML = 'Launch PIP';

            let overloadedBarClosingButton = document.createElement('span');
            overloadedBarClosingButton.style.fontSize = '2rem';
            overloadedBarClosingButton.style.color = 'white';
            overloadedBarClosingButton.style.cursor = 'pointer';
            overloadedBarClosingButton.style.margin = '16px';
            overloadedBarClosingButton.innerHTML = 'X';

            overloadedBarClosingButton.setAttribute('id', 'close-overloaded-bar-button');
            overloadedBar.prepend(overloadedBarClosingButton);
            overloadedBar.prepend(pipButton);

            body.prepend(overloadedBar);

            document.getElementById('button-setpip').onclick = function() {
                let video = document.getElementsByTagName('video')[0];
                if (video !== document.pictureInPictureElement) {
                    video.requestPictureInPicture();
                    pipButton.innerHTML = 'Stop PIP';
                    pipButton.style.backgroundColor = '#e34444';
                }
                else {
                    document.exitPictureInPicture();
                    pipButton.innerHTML = 'Relaunch PIP';
                    pipButton.style.backgroundColor = '#34a1eb';
                }
            };

            document.getElementById('close-overloaded-bar-button').onclick = function() {
                 overloadedBar.remove();
            };
        }
    }, 3000)
})();